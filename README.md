# RIPE Infrastructure

The RIPE Infrastructure is hosted on AWS and run pipeline CI CD on Gitlab.

## Repository and files layout  
- [EKS](Amazon Elastic Kubernetes ) AWS infrastructure as code. Separate readme available in this folder.
    - [](./EKS/eks-cluster.tf) EKS Cluster Resources
        * IAM Role to allow EKS service to manage other AWS services
        * EC2 Security Group to allow networking traffic with EKS cluster
        * EKS Cluster
    - [](./EKS/eks-worker-nodes.tf)  EKS Worker Nodes Resources
        * IAM role allowing Kubernetes actions to access other AWS services
        * EKS Node Group to launch worker nodes
    - [](./EKS/vpc.tf)   VPC Resources
        * VPC
        * Subnets
        * Internet Gateway
        * Route Table
    - [](./EKS/vpc.tf)   VPC Resources
        * VPC
        * Subnets
        * Internet Gateway
        * Route Table    
- [Dockerfile](./Dockerfile) The Dockerfile is essentially the build instructions to build the image.
- [Deployment](./deployment.yaml) The A deployment is an object in Kubernetes that lets you manage a set of identical pods.
- [gitlab-ci](./gitlab-ci.yaml) GitLab detects the changes on the project  and an application called GitLab Runner runs the scripts defined in the jobs.

 
     
To work with this repo localy make: git clone  https://gitlab.com/tao-devops/ripe.git
