# RIPE EKS AWS Terraform
This repo contains a Terraform  for creating Kubernetes clusters on , Amazon Web Services(AWS).

## What's in this repo

* [EKS](./EKS/README.md): Amazon cloud terraform files to create kubernetes cluster using EKS and VCP.
    - [](./EKS/eks-cluster.tf) EKS Cluster Resources
        * IAM Role to allow EKS service to manage other AWS services
        * EC2 Security Group to allow networking traffic with EKS cluster
        * EKS Cluster
    - [](./EKS/eks-worker-nodes.tf)  EKS Worker Nodes Resources
        * IAM role allowing Kubernetes actions to access other AWS services
        * EKS Node Group to launch worker nodes
    - [](./EKS/vpc.tf)   VPC Resources
        * VPC
        * Subnets
        * Internet Gateway
        * Route Table
    - [](./EKS/vpc.tf)   VPC Resources
        * VPC
        * Subnets
        * Internet Gateway
        * Route Table    


## What is Kubernetes?
[Kubernetes](https://kubernetes.io/) is an open source container management system for deploying, scaling, and managing containerized applications. Kubernetes is built by Google based on their internal proprietary container management systems . Kubernetes provides a cloud agnostic platform to deploy your containerized applications with built in support for common operational tasks such as replication, autoscaling, self-healing, and rolling deployments.

## What is Managed Kubernetes services?
Managed Kubernetes is when third-party providers take over responsibility for some or all of the work necessary for the successful set-up and operation of K8s. Depending on the vendor, “managed” can refer to anything from dedicated support, to hosting with pre-configured environments, to full hosting and operation. We will   EKS 

## What is Terraform?
Terraform is a tool for building, changing, and versioning infrastructure safely and efficiently. Terraform can manage existing and popular service providers as well as custom in-house solutions.  

## Prerequisites
 
## Create AWS resources

* Creating this project by running localy.

* Set up environment on your machine before running the terraform commands. use the following links to setup your machine.
    *  Install AWS cli https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
    *  Install Terraform https://learn.hashicorp.com/tutorials/terraform/install-cli
    *  Create a user on AWS account and take the KEY and SECRET values and it is important that the user to have AdministratorAccess Permision            https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html
    * 

* How to create EKS cluster resources by using the terraform command
Before using the terraform commands login to aws

```CMD

aws configure
      AWS Access Key ID [****************FGWH]:
      AWS Secret Access Key [****************faxa]:
      Default region name [us-east-1]:
      Default output format [None]:

 
```
###  Run terraform commnad to create a Kubernetes cluster on AWS

```console
# cd ./EKS
terraform init  

```console
# cd ./EKS
terraform validate  

```console
# cd ./EKS
terraform plan 

```console
# cd ./EKS
terraform apply  
     

       
